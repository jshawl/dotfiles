execute pathogen#infect()
autocmd VimEnter * wincmd p 
syntax on

" Add full file path to your existing statusline
set hlsearch
set noswapfile
set showcmd
set ignorecase
set smartcase
set splitright
set number
set autoindent
set ls=2
set shiftwidth=2
set wmw=0                     " set the min width of a window to 0 so we can maximize others 
set wmh=0                     " set the min height of a window to 0 so we can maximize others
colorscheme mustang
" Cursorline {{{
" Only show cursorline in the current window and in normal mode.
augroup cline
    au!
    au WinLeave,InsertEnter * set nocursorline
    au WinEnter,InsertLeave * set cursorline
augroup END

" Easy buffer navigation
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
map <C-+> <C-w><bar><C-w>_
noremap <C-w>f <c-w><bar><c-w>_

filetype on           " Enable filetype detection
filetype indent on    " Enable filetype-specific indenting
filetype plugin on    " Enable filetype-specific plugins

function! s:zen_html_tab() "emmet tab expansion
  let line = getline('.')
  if match(line, '<.*>') >= 0
    return "\<c-y>n"
  endif
  return "\<c-y>,"
endfunction
autocmd FileType html,php imap <buffer><expr><tab> <sid>zen_html_tab()

" disable arrow keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" easy indenting
noremap > >>
noremap < <<
vnoremap > >gv
vnoremap < <gv

" quick escape
inoremap jj <Esc>  

" keep the cursor centered on screen
set scrolloff=999
